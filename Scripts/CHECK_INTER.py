import sys,os,pickle
from LOAD_SAVE import *

# get values that are in lst1 and lst 2
def getIntersectionKeys (lst1,lst2):
    lst = [value for value in lst1 if value in lst2]
    return lst

I = load_obj('imp_words')
I = list(I.keys())
with open ('softwords.txt') as f:
    S = f.readlines()
for i in range (len(S)):
    S[i] = S[i][:-1]

L = sorted(getIntersectionKeys(I,S))
print (len(L),len(S),len(I))

for w in L:
    print (w)