import os,random,time,sys,math
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable

EMBEDDING_SIZE = 200
N_HIDDEN = 25
N_CATEGORIES = 2
N_ITERS = 40000

wordVecFile = sys.argv[1]

class Net(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size) 
        self.sigm = nn.Sigmoid()
        self.fc2 = nn.Linear(hidden_size, num_classes)  
        self.softmax = nn.LogSoftmax(dim=0)
        self.num_classes = num_classes
    def forward(self, x):
        out = self.fc1(x)
        out = self.sigm(out)
        out = self.fc2(out)
        out = self.softmax(out).view(1,self.num_classes)
        return out

def createWordVecDict (filename):
    D = {}
    with open (filename, "r") as file:     
        data = file.readlines()[1:]
    for i in range (len(data)):
        temp = data[i].split()
        word = temp[0]
        vec = np.asarray([float(x) for x in temp[1:]])
        D[word] = vec
    return D

def vecToTensor (v):
    tensor = torch.from_numpy(v).float()
    return tensor

def getDocVec (filename):
    with open (filename, "r") as file:
        data = file.read()
    data = data.split()
    c = 0
    X = np.zeros(EMBEDDING_SIZE)
    for word in data:
        if (word in D):
            X = X + D[word]
            c += 1
    return (X/float(c))

def randomTrainingExample ():
    cat = random.randint(0,1) # which newspaper?
    newspaper = "hindu" if cat==0 else "indian"
    art_dir = "../../Stuff/"+newspaper+"/train/"
    file = random.choice(os.listdir(art_dir))
    line_tensor = Variable(vecToTensor(getDocVec(art_dir+file)))
    cat_tensor = Variable(torch.LongTensor([cat]))
    return cat, cat_tensor, line_tensor

def categoryFromOutput(output):
    top_n, top_i = output.data.topk(1) # Tensor out of Variable with .data
    category_i = top_i[0][0]
    if (category_i == 0):
        p = "hindu"
    else:
        p = "indian"
    return p, category_i

def train (category_tensor, line_tensor):
    optimizer.zero_grad()
    output = ffnet(line_tensor)
    loss = criterion(output, category_tensor)
    loss.backward()
    optimizer.step()
    return output, loss.data[0]

def timeSince(since):
    now = time.time()
    s = now - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

D = createWordVecDict(wordVecFile)
ffnet = Net(EMBEDDING_SIZE, N_HIDDEN, N_CATEGORIES)
learning_rate = 0.001
criterion = nn.NLLLoss() 
optimizer = torch.optim.Adam(ffnet.parameters(), lr=learning_rate)
current_loss = 0
all_losses = []
print_every = 500

start = time.time()
good = 0; bad = 0;

for iter in range(1, N_ITERS + 1):
    category, category_tensor, line_tensor = randomTrainingExample()
    output, loss = train(category_tensor, line_tensor)
    current_loss += loss

    guess, guess_i = categoryFromOutput(output)
    if (guess_i == category):
        correct = '✓'; good += 1;
    else:
        correct = '✗'; bad += 1;

    if iter % print_every == 0:
        print (iter, timeSince(start), loss, guess, correct)
    

print (good, bad)

torch.save (ffnet, 'ff_vM.net')

