

def getList (fname):
    A = {}
    with open (fname) as f:
        D = f.readlines()
    for line in D:
        if (line == '\n'):
            continue
        i = line.find(',')
        n = float(line[:i])
        w = line[i+2:-1]
        if (n < -0.1 or n > 0.1):
            A[w] = n
    return A


a1 = getList('wv_hindu_new.txt')
a2 = getList('wv_indian_new.txt')

b = list(a1.keys()) + list(a2.keys())

print (b)
for w in b:
    print (w)