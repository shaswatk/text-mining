import os,random,time,sys,math, pickle
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from LOAD_SAVE import *

indir = sys.argv[1]
wordVecFile = sys.argv[2]
model_file = sys.argv[3]

class Net(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size) 
        self.sigm = nn.Sigmoid()
        self.fc2 = nn.Linear(hidden_size, num_classes)  
        self.softmax = nn.LogSoftmax(dim=0)
        self.num_classes = num_classes
    def forward(self, x):
        out = self.fc1(x)
        out = self.sigm(out)
        out = self.fc2(out)
        out = self.softmax(out).view(1,self.num_classes)
        return out

def vecToTensor (v):
    tensor = torch.from_numpy(v).float()
    return tensor

def createWordVecDict (filename):
    D = {}
    with open (filename, "r") as file:     
        data = file.readlines()[1:]
    for i in range (len(data)):
        temp = data[i].split()
        word = temp[0]
        vec = np.asarray([float(x) for x in temp[1:]])
        D[word] = vec
    return D

def testVecRat (doc, D, ratio):
    V = []
    for word in doc:
        if (word in D):
            V.append(D[word])
        elif (word+'1' in D and word+'2' in D):
            t1 = D[word+'1']; t2 = D[word+'2'];
            t = (ratio[word]*t1 + t2)/(ratio[word]+1)
            V.append(t)
        elif (word+'1' in D):
            V.append(D[word+'1'])
        elif (word+'2' in D):
            V.append(D[word+'2'])
    vec = np.mean(np.asarray(V),axis=0)
    return vec

def categoryFromOutput(output):
    top_n, top_i = output.data.topk(1) # Tensor out of Variable with .data
    category_i = top_i[0][0]
    if (category_i == 0):
        p = "hindu"
    else:
        p = "indian"
    return p, category_i

D = createWordVecDict(wordVecFile)
ffnet = torch.load(model_file)
rat_dict = load_obj('imp_words')

c1 = 0; c2 = 0;
for fx in os.listdir(indir):
    filename = indir+fx
    # A = []
    with open (filename, 'r') as f:
        doc = f.read().split()
        tv = Variable(vecToTensor(testVecRat(doc,D,rat_dict)))
        output = ffnet(tv)
        guess, guess_i = categoryFromOutput(output)
        if (guess == 'hindu'):
            c1 += 1
        else:
            c2 += 1

print (c1,c2)
