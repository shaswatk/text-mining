import os,random,time,sys,math, pickle
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from LOAD_SAVE import *

indir = sys.argv[1]
wordVecFile = sys.argv[2]
model_file = sys.argv[3]

class Net(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size) 
        self.sigm = nn.Sigmoid()
        self.fc2 = nn.Linear(hidden_size, num_classes)  
        self.softmax = nn.LogSoftmax(dim=0)
        self.num_classes = num_classes
    def forward(self, x):
        out = self.fc1(x)
        out = self.sigm(out)
        out = self.fc2(out)
        out = self.softmax(out).view(1,self.num_classes)
        return out

def vecToTensor (v):
    tensor = torch.from_numpy(v).float()
    return tensor

def createWordVecDict (filename):
    D = {}
    with open (filename, "r") as file:     
        data = file.readlines()[1:]
    for i in range (len(data)):
        temp = data[i].split()
        word = temp[0]
        vec = np.asarray([float(x) for x in temp[1:]])
        D[word] = vec
    return D

def testVecRat (doc, D, ratio):
    V = []
    for word in doc:
        if (word in D):
            V.append(D[word])
        elif (word+'1' in D and word+'2' in D):
            t1 = D[word+'1']; t2 = D[word+'2'];
            t = (ratio[word]*t1 + t2)/(ratio[word]+1)
            V.append(t)
        elif (word+'1' in D):
            V.append(D[word+'1'])
        elif (word+'2' in D):
            V.append(D[word+'2'])
    vec = np.mean(np.asarray(V),axis=0)
    return vec

def categoryFromOutput(output):
    top_n, top_i = output.data.topk(1) # Tensor out of Variable with .data
    category_i = top_i[0][0]
    if (category_i == 0):
        p = "hindu"
    else:
        p = "indian"
    return p, category_i

D = createWordVecDict(wordVecFile)
ffnet = torch.load(model_file)
rat_dict = load_obj('imp_words')

c1 = 0; c2 = 0;
c = 0
offset = 5

Z_pos = []; Z_neg = [];
Z_word_pos = []; Z_word_neg = [];

for fx in os.listdir(indir):
    c += 1
    if (c == 1501):
        break
    if (c%100 == 0):
        print (c)
    filename = indir+fx
    A = []
    with open (filename, 'r') as f:
        doc = f.read().split()
        tv = Variable(vecToTensor(testVecRat(doc,D,rat_dict)))
        output = ffnet(tv)
        o = output.data.numpy().flatten()
        A.append(o)
    for k in range (0,len(doc)):
        temp_doc = doc[:k] + doc[k+1:]
        tv = Variable(vecToTensor(testVecRat(temp_doc,D,rat_dict)))
        output = ffnet(tv)
        o = output.data.numpy().flatten()
        A.append(o)
    A = np.power(math.exp(1),A)

    B = []; B_ind = [];
    for i in range (1,len(A)):
        B.append(A[i][0]-A[0][0])

    B_ind = np.argsort(B)
    B = np.sort(B)

    for i in range (0,offset):
        if (i > 0 and ((len(Z_pos) > 0) and Z_pos[-1] == B[i]) or ((len(Z_neg) > 0 and Z_neg[-1] == B[-offset+i]))):
            continue
        Z_pos.append(B[i])
        if (B[i] <= -0.2):
            w = doc[B_ind[i]]
            if (w[:5] == 'marxi'):
                print ('yahooo!!!!')
            print ('           ')
            print (filename)
            print(w)
            print ('         ')
        Z_neg.append(B[-offset+i])
        Z_word_pos.append(doc[B_ind[i]])
        Z_word_neg.append(doc[B_ind[-offset+i]])

    # print(Z_word_pos)
    # print(Z_word_neg)

R_pos = list(zip(Z_pos,Z_word_pos)); R_neg = list(zip(Z_neg,Z_word_neg))

R_pos = sorted(R_pos)
R_neg = sorted(R_neg, reverse=True)

print (R_pos)
print (R_neg)

# Z_word_sort = []
# Z_ind = np.argsort(Z)
# for t in Z_ind:
#     Z_word_sort.append(Z_word[t])
# # print (np.shape(Z_ind))
# # print (Z_word)
# # Z_word_sort = Z_word[Z_ind]
# # print (Z_word_sort)

# with open("wv_indian_new.txt", "w") as f:
#     for s in R_pos:
#         f.write(str(s[0]) + ", " + str(s[1]) +"\n")
#     f.write("\n\n")
#     for s in R_neg:
#         f.write(str(s[0]) + ", " + str(s[1]) +"\n")

# # print (c1,c2)
