# Usage : python3 create_dict.py <corpus_dir_path> <dict_name>

import sys,os,pickle
from nltk.tokenize import word_tokenize
from LOAD_SAVE import *

corpus_dir = sys.argv[1]
dict_name = sys.argv[2]
word_set = {}

def createDict (filename):
    for fx in os.listdir(corpus_dir):
        filename = corpus_dir+fx
        with open (filename, "r") as file:
            raw_text = file.read()
        words = word_tokenize(raw_text)
        for i,w in enumerate(words):
            if w not in word_set:
                word_set[w] = 1
            else:
                word_set[w] += 1
    return word_set

word_dict = createDict (corpus_dir)
save_obj(word_dict,dict_name)
