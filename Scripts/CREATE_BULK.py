import os
import sys

curr_dir = sys.argv[1]
outputfile = sys.argv[2]


data = []
s = ''
for filename in os.listdir(curr_dir):
    with open (curr_dir+filename, "r") as file:
        data.append(file.read().replace('\n', ' '))
        s += data[-1]
f = open(outputfile, 'wt', encoding='utf-8')
f.write(s)
f.close()