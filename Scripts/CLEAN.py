# Usage : python3 change.py <dir>
# Cleans all files in the directory

import fileinput, re, os, sys, pickle

dirpath = sys.argv[1]
for filename in os.listdir(dirpath): 
	S = ''
	with open(dirpath+filename, 'r+') as f:
		for line in f:
			line = re.sub(r'\<(.*?)\>',"", line.rstrip())
			line = re.sub(r'\\n',"", line.rstrip())
			line = re.sub(r'\\r',"", line.rstrip())
			line = re.sub(r'\\\'',"\'", line.rstrip())
			line = re.sub("... contd.","", line.rstrip())
			line = re.sub(r'\.',".\n", line.rstrip())
			line = re.sub(r'[^\w\s]',' ',line.rstrip())
			line = line.lower()
			S = S + line
	f.close()
	with open (dirpath+filename, 'w') as f:
		f.write (S)
	f.close()
