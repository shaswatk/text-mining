# Usage: python3 create_intersection.py <dict_1_name> <dict_2_name>
# The first two parameters must relate to existing pkl files.

import sys,os,pickle
from LOAD_SAVE import *

dict_1_name = sys.argv[1]
dict_2_name = sys.argv[2]

# get values that are in lst1 and lst 2
def getIntersectionKeys (lst1,lst2):
    lst = [value for value in lst1 if value in lst2]
    return lst

def normalize (cf):
    ratio = {}; c = 0;
    for word in cf:
        c += cf[word]
    for word in cf:
        ratio[word] = cf[word] / float(c)
    return ratio

dict_1 = load_obj(dict_1_name)
dict_2 = load_obj(dict_2_name)

keys_1 = list(set(dict_1.keys()))
keys_2 = list(set(dict_2.keys()))
intersection = getIntersectionKeys (keys_1,keys_2)
I = {}

ratio_1 = normalize(dict_1)
ratio_2 = normalize(dict_2)

for w in intersection:
    I[w] = float(ratio_1[w])/float(ratio_2[w]) # create intersection ratio list

save_obj(I,'imp_words')