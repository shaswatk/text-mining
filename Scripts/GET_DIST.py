# This code will find out distances between common words

import sys,os,pickle
import numpy as np
from LOAD_SAVE import *


def createWordVecDict (filename):
    D = {}
    with open (filename, "r") as file:     
        data = file.readlines()[1:]
    for i in range (len(data)):
        temp = data[i].split()
        word = temp[0]
        vec = np.asarray([float(x) for x in temp[1:]])
        D[word] = vec
    return D

def getPairs (num, l):
	P = []
	for i in range (0, NUM_RAND_PAIRS):
		a = np.random.randint(0,len(l))
		b = np.random.randint(0,len(l))
		P.append((l[a],l[b]))
	return P

def getPairDist (pair_list, f):
	for p in pair_list:
		r1 = I[p[0]]; r2 = I[p[1]];
		if not (p[0]+'1' in embed_dict and p[0]+'2' in embed_dict and p[1]+'1' in embed_dict and p[1]+'2' in embed_dict):
			continue
		d1 = np.linalg.norm(embed_dict[p[0]+'1'] - embed_dict[p[0]+'2']);
		d2 = np.linalg.norm(embed_dict[p[1]+'1'] - embed_dict[p[1]+'2']);
		d = d1/d2; r = r1/r2;
		if (f == 'indian'):
			r = 1.0/r
		print (r, d, d/r)

I = load_obj ('imp_words')
imp_big = []; imp_small = []
for w in I:
	r = I[w]
	if (r > 2):
		imp_big.append(w)
	if (r < 0.5):
		imp_small.append(w)
	# print (w,I[w])
# print (len(I))
# print (len(imp_big), len(imp_small)) # Sudden revelation
embed_dict = createWordVecDict(sys.argv[1])

NUM_RAND_PAIRS = 543

small_pairs = getPairs(NUM_RAND_PAIRS, imp_small)
big_pairs = getPairs(NUM_RAND_PAIRS, imp_big)

getPairDist(small_pairs, 'hindu')
print ()
getPairDist(big_pairs, 'indian')