# Usage : python3 TAG.py <dirpath> <tag>

import re,sys,os,pickle
from LOAD_SAVE import *

dirpath = sys.argv[1]
tag = sys.argv[2]
freq_ratio = load_obj('imp_words')

S = ''
for fx in os.listdir(dirpath):
	filename = dirpath+fx
	print (filename)
	with open (filename, 'r') as f:
		for line in f:
			for w in line.split():
				if w in freq_ratio:
					if freq_ratio[w] > 2 or freq_ratio[w] < 0.5:
						w += tag
				else:
					w += tag
				S += w + ' '
			S += '\n'
	f.close()
	with open (filename, 'w') as f:
		f.write (S)
	f.close()
	S = ''
