import sys,os,pickle,random
import numpy as np
from scipy.spatial.distance import cosine as cosdist
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from LOAD_SAVE import *

clf_file = sys.argv[1]
wordVecFile = sys.argv[2]
indir = sys.argv[3]
EMBEDDING_DIM = 200

clf = load_obj(clf_file)

def createWordVecDict (filename):
    D = {}
    with open (filename, "r") as file:     
        data = file.readlines()[1:]
    for i in range (len(data)):
        temp = data[i].split()
        word = temp[0]
        vec = np.asarray([float(x) for x in temp[1:]])
        D[word] = vec
    return D

def testVecRat (doc, D, ratio):
    V = []
    for word in doc:
        if (word in D):
            V.append(D[word])
        elif (word+'1' in D and word+'2' in D):
            t1 = D[word+'1']; t2 = D[word+'2'];
            t = (ratio[word]*t1 + t2)/(ratio[word]+1)
            V.append(t)
        elif (word+'1' in D):
            V.append(D[word+'1'])
        elif (word+'2' in D):
            V.append(D[word+'2'])
    vec = np.mean(np.asarray(V),axis=0)
    return vec

D = createWordVecDict(wordVecFile)
rat_dict = load_obj('imp_words')

tV = []
for fx in os.listdir(indir):
  filename = indir+fx
  with open (filename, 'r') as f:
      doc = f.read().split()
  tV.append(testVecRat(doc,D,rat_dict))

ans = clf.predict(tV)

c1=0; c2=0;
for x in ans:
  if (x == 1):
      c1 += 1
  else:
      c2 += 1

print (c1,c2)