# Usage : python3 DOC2VEC.py ../Stuff/indian/test/ ../word2vec_pytorch-master/vectors.txt ../Stuff/hindu/train/ ../Stuff/indian/train/

import sys
import os
import numpy as np
from scipy.spatial.distance import cosine as cosdist

indir = sys.argv[1]
wordVecFile = sys.argv[2]
corpusDir1 = sys.argv[3]
corpusDir2 = sys.argv[4]
EMBEDDING_DIM = 200

def createWordVecDict (filename):
    D = {}
    with open (filename, "r") as file:     
        data = file.readlines()[1:]
    for i in range (len(data)):
        temp = data[i].split()
        word = temp[0]
        vec = np.asarray([float(x) for x in temp[1:]])
        D[word] = vec
    return D

def getDocVec (filename):
    with open (filename, "r") as file:
        data = file.read()
    data = data.split()
    c = 0
    X = np.zeros(EMBEDDING_DIM)
    for word in data:
        if (word in D):
            X = X + D[word]
            c += 1
    return (X/float(c))

def constructDocVecs (corpus_dir):
    X = []
    for folder in os.listdir(corpus_dir):
        for fx in os.listdir(corpus_dir+folder):
            doc = corpus_dir+folder+'/'+fx
            X.append(getDocVec(doc))
    return X

def getDist (docVecs,tV):
    s = 0;
    for dV in docVecs:
        s += cosdist(tV,dV)
    return (s/float(len(docVecs)))

D = createWordVecDict(wordVecFile)
X1 = constructDocVecs(corpusDir1)
X2 = constructDocVecs(corpusDir2)

for fx in os.listdir(indir+'sample1/'):
    doc1 = indir+'sample1/'+fx
    doc2 = indir+'sample2/'+fx
    tV1 = getDocVec(doc1)
    tV2 = getDocVec(doc2)
    d1 = getDist(X1,tV1); d2 = getDist(X2,tV2)
    if (d1 < d2):
        print ("Hindu"); c1 += 1;
    else:
        print ("Indian"); c2 += 1;

print (c1,c2)