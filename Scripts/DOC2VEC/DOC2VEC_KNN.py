# Usage : python3 DOC2VEC_KNN.py ../Stuff/indian/sample/ RNN/vectors.bin ../Stuff/hindu/train/ ../Stuff/indian/train/
import sys,os,pickle,math
import numpy as np
from scipy.spatial.distance import cosine as cosdist
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from LOAD_SAVE import *

indir = sys.argv[1]
wordVecFile = sys.argv[2]
corpusDir1 = sys.argv[3]
corpusDir2 = sys.argv[4]
EMBEDDING_DIM = 200

def createWordVecDict (filename):
    D = {}
    with open (filename, "r") as file:     
        data = file.readlines()[1:]
    for i in range (len(data)):
        temp = data[i].split()
        word = temp[0]
        vec = np.asarray([float(x) for x in temp[1:]])
        D[word] = vec
    return D

def getDocVec (fiz, t):
	if (t == 'file'):
		with open (fiz, "r") as file:
			data = file.read().split()
	else:
		data = fiz
	c = 0
	X = np.zeros(EMBEDDING_DIM)
	for word in data:
		if (word in D):
			X = X + D[word]
			c += 1
	return (X/float(c))

def constructDocVecs (corpus_dir):
    X = []
    for fx in os.listdir(corpus_dir):
            doc = corpus_dir+fx
            X.append(getDocVec(doc,'file'))
    return X

def tagTest (doc, D, ratio):
	good_doc = []
	for word in doc:
		if (word in D):
			good_doc.append(word)
		elif (word+'1' in D and word+'2' in D):
			if (ratio[word] > 2):
				good_doc.append(word+'1')
			elif (ratio[word] < 0.5):
				good_doc.append(word+'2')
		elif (word+'1' in D):
			good_doc.append(word+'1')
		elif (word+'2' in D):
			good_doc.append(word+'2')
	return good_doc

def testVecRat (doc, D, ratio):
	V = []
	for word in doc:
		if (word in D):
			V.append(D[word])
		elif (word+'1' in D and word+'2' in D):
			t1 = D[word+'1']; t2 = D[word+'2'];
			t = (ratio[word]*t1 + t2)/(ratio[word]+1)
			V.append(t)
		elif (word+'1' in D):
			V.append(D[word+'1'])
		elif (word+'2' in D):
			V.append(D[word+'2'])
	vec = np.mean(np.asarray(V),axis=0)
	return vec

def getRandomSamples(X,y,s,n):
	r = np.random.choice(s,n)
	return ([X[a] for a in r],[y[a] for a in r])

D = createWordVecDict(wordVecFile)
X1 = constructDocVecs(corpusDir1); y1 = [1]*len(X1);
X2 = constructDocVecs(corpusDir2); y2 = [2]*len(X2);
rat_dict = load_obj('imp_words')

X1,y1 = getRandomSamples(X1,y1,len(y1),10000)
X2,y2 = getRandomSamples(X2,y2,len(y2),10000)
X = X1 + X2; y = y1 + y2;

neigh = KNeighborsClassifier(n_neighbors=5, n_jobs=4, algorithm='auto')
neigh.fit(X, y)

tV = []
for fx in os.listdir(indir):
	filename = indir+fx
	with open (filename, 'r') as f:
		doc = f.read().split()
	# good_doc = tagTest(doc,D,rat_dict)
	# tV.append(getDocVec(good_doc,'list'))
	tV.append(testVecRat(doc,D,rat_dict))

ans = neigh.predict(tV)

c1=0; c2=0;
for x in ans:
	if (x == 1):
		c1 += 1
	else:
		c2 += 1

print (c1,c2)
