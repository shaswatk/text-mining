# Usage : python3 STEM.py <infile> <outfile>

import sys,os,pickle
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from LOAD_SAVE import *

filename = sys.argv[1]
outfile = sys.argv[2]

with open (filename, "r") as file:
    s = file.read()
ps = PorterStemmer()
words = word_tokenize(s)
s = ''
for word in words:
    s += ps.stem(word) + ' '
with open (outfile, 'w') as f:
    f.write (s)
f.close()