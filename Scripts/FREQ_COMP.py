import sys,os,pickle
from LOAD_SAVE import *

def normalize (cf):
    ratio = {}; c = 0;
    for word in cf:
        c += cf[word]
    for word in cf:
        ratio[word] = cf[word] / float(c)
    return ratio

def classify (filename):
    freq = {}; c = 0; ratio = {}
    with open (filename, "r") as file:
        data = file.read()
    for line in data.split():
        for word in line.split():
            c += 1
            if word in freq:
                freq[word] += 1
            else:
                freq[word] = 1
    for word in freq:
        ratio[word] = freq[word] / float(c)

    c1 = 0; c2 = 0;

    for word in ratio:
        if (word in ratio_1 and word in ratio_2):
            if (abs (ratio_1 [word] - ratio [word]) < abs (ratio_2 [word]-ratio [word])):
                c1 += 1
            else:
                c2 += 1
    if (c1 > c2):
        return 1
    else:
        return 2

curr_dir = sys.argv[1]
cf1 = load_obj ('dict_1'); cf2 = load_obj ('dict_2')
ratio_1 = normalize (cf1); ratio_2 = normalize (cf2)
c1 = 0; c2 = 0

for filename in os.listdir(curr_dir):
    f = curr_dir + filename
    t = classify(f)
    if (t == 1):
        c1 += 1
    else:
        c2 += 1

print (c1, c2)