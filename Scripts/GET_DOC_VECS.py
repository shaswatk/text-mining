import sys,os,pickle,random
import numpy as np
from LOAD_SAVE import *

wordVecFile = sys.argv[1]
corpusDir1 = sys.argv[2]
corpusDir2 = sys.argv[3]
EMBEDDING_DIM = 200

def createWordVecDict (filename):
    D = {}
    with open (filename, "r") as file:     
        data = file.readlines()[1:]
    for i in range (len(data)):
        temp = data[i].split()
        word = temp[0]
        vec = np.asarray([float(x) for x in temp[1:]])
        D[word] = vec
    return D

def getDocVec (fiz, t):
    if (t == 'file'):
        with open (fiz, "r") as file:
            data = file.read().split()
    else:
        data = fiz
    c = 0
    X = np.zeros(EMBEDDING_DIM)
    for word in data:
        if (word in D):
            X = X + D[word]
            c += 1
    return (X/float(c))

def constructDocVecs (corpus_dir):
    X = []
    for fx in os.listdir(corpus_dir):
            doc = corpus_dir+fx
            X.append(getDocVec(doc,'file'))
    return X

def getRandomSamples(X,y,s,n):
    r = np.random.choice(s,n)
    return ([X[a] for a in r],[y[a] for a in r])

D = createWordVecDict(wordVecFile)
X1 = constructDocVecs(corpusDir1); y1 = [1]*len(X1);
X2 = constructDocVecs(corpusDir2); y2 = [2]*len(X2);

X1,y1 = getRandomSamples(X1,y1,len(y1),500)
X2,y2 = getRandomSamples(X2,y2,len(y2),500)

X = X1 + X2; y = y1 + y2;

save_obj((X,y), 'docvecs')
